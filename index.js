// const wsclient=  require('websocket').client;
// const {RTCPeerConnection, RTCSessionDescription} = window

const sendProtocolMsg = (ws, id, message) => {
    const toSend = {
        'id': id,
        'message': message,
        'type': 'protocol'
    }
    ws.send(JSON.stringify(toSend));
};

const generateID = () => {
    return Math.random().toString(16).substring(0, 4);
};

let dc = null
let ican = 0;
let irem = 0;

const button = document.querySelector('button#sendbutton');
const input = document.querySelector('#tosend');

input.value = `{
   "path": "/",
   "id": "${generateID()}",
   "requestData": 
      {
         "method": "getnodeinfo",
         "params": []
      }
}`;
const output = document.querySelector('#nodemsg');
output.value = 'Result will be here';
const result = document.querySelector('#result');
const p = document.querySelector('#status');
button.onclick = function () {
    if (!dc) {
        result.textContent = 'Not connected!'
        return;
    }
    result.textContent = 'Waiting...'
    dc.send(input.value);
};

async function setupPC(connection, id)
{
    const pc = new RTCPeerConnection(config);
    pc.addEventListener('icecandidate', event => {
        if (event.candidate) {
            console.log('candidata' + ican++)
            console.log(event.candidate);

            if (event.candidate.candidate)
            sendProtocolMsg(connection, id, {type: 'candidate', 'candidate': event.candidate.candidate, 'mid': event.candidate.sdpMid});
        }
    });
    pc.addEventListener('connectionstatechange', async (event) => {
        console.log('new connection state: ' + pc.connectionState)
        if (pc.connectionState === 'connected') {
            console.log('Connected peer with id: ' + id);
        } else if (pc.connectionState === 'failed') {
            p.textContent = 'Failed to connect';
        } else if (pc.connectionState === 'closed' || pc.connectionState == 'disconnected') {
            p.textContent = 'Connection closed';
        }
    });
    const dataChannel = await pc.createDataChannel('rpc'); // 'rpc' for rpc datachannel. Websocket stuff should have different data channel
    dataChannel.addEventListener('open', event => {
        
        dc = dataChannel;
        p.textContent = 'Ready to send rpc!';
    });
    dataChannel.addEventListener('message', event => {
        const message = event.data;
        result.textContent = 'Success!'
        output.value = message;
    });

    return pc;
}

const config = {'iceServers': [{'urls': 'stun:stun.l.google.com:19302'},
                                // {'urls': 'stun:stun3.l.google.com:19302'},
                                ]};
const client = new WebSocket('ws://95.216.28.118:13131/signaling'); // IP of signaling node + /signaling
client.addEventListener('open', async (event)=> {
    const connection = client;
    const id = '80.234.78.52:3798'; // IP of private node
    
    const pc = await setupPC(connection, id);
    
    connection.addEventListener("message", async (event)=> {
        const msg_raw = event.data;
        console.log('Message: ' + msg_raw);
        const message = JSON.parse(msg_raw);
        if (message.type == 'protocol') {
            const msg = message.message;
            if (msg.type == "answer") {
                console.log('Gotcha answer')
                const remoteDesc = new RTCSessionDescription(msg);
                await pc.setRemoteDescription(remoteDesc);
            } else if (msg.type == "candidate") {
                // TODO: also mid available - message.mid
                console.log("Remote" + irem++);
                console.log(msg.candidate)
                await pc.addIceCandidate({'candidate': msg.candidate, 'sdpMid': msg.mid});
            }
        } else {
            console.log("Unexpected message");
        }
    });

    const offer = await pc.createOffer();
    await pc.setLocalDescription(offer);
    sendProtocolMsg(connection, id, offer);
})
